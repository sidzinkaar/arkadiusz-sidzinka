#ifndef Point2D_h
#define Point2D_h

#include<iostream>
#include<cstring>
#include<cmath>
#include<iostream>

class Point2D
{
	public :
		Point2D();
		Point2D(float x, float y);
		~Point2D (void);
		const Point2D operator+(const Point2D &right_operand) const;
		const Point2D operator-(const Point2D &right_operand) const;
		const Point2D operator/(float xy) const;
		const Point2D operator*(float xy) const;
		const void operator+=(const Point2D &right_operand);
		const void operator-=(const Point2D &right_operand);
		const void operator*=(float xy);
		const void operator/=(float xy);
		const bool operator==(const Point2D &right_operand);
		friend std::ostream &operator<<(std::ostream& os, Point2D& c);
		friend std::istream &operator>>(std::istream& is, Point2D& c);
		operator double();
		
	private :
		float x;
		float y;
};
#endif
