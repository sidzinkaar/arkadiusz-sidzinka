#include "Point2D.h"

Point2D::operator double()
{
	return sqrt((this->x * this->x) + (this->y * this->y));
}


Point2D::Point2D()
{
	x=0;
	y=0;
}

const Point2D Point2D::operator+ (const Point2D &right_operand) const 
{
	return Point2D(x + right_operand.x, y + right_operand.y);
}

const Point2D Point2D::operator- (const Point2D &right_operand) const 
{
	return Point2D(x - right_operand.x, y - right_operand.y);
}

const Point2D Point2D::operator/(float xy) const
{
	return Point2D(x / xy, y / xy);
}

const Point2D Point2D::operator*(float xy) const
{
	return Point2D(x * xy, y * xy);
}

const void Point2D::operator+=(const Point2D &right_operand)
{
	this->x += right_operand.x;
	this->y += right_operand.y;
}

const void Point2D::operator-=(const Point2D &right_operand)
{
	this->x -= right_operand.x;
	this->y -= right_operand.y;
}

const void Point2D::operator*=(float xy)
{
	this->x *= xy;
	this->y *= xy;
}

const void Point2D::operator/=(float xy)
{
	this->x /= xy;
	this->y /= xy;
}

const bool Point2D::operator==(const Point2D &right_operand)
{
	if(this->x == right_operand.x && this->y == right_operand.y) return true;
	else return false;
}


Point2D::~Point2D()
{
}

Point2D::Point2D(float x, float y)
{
	this->x = x;
	this->y = y;
}

std::ostream &operator<<(std::ostream& os, Point2D& c)
{
    os << "(" << c.x << ", " << c.y << ")";
    return os;
}

std::istream &operator>>(std::istream& is, Point2D& c)
{
	is >> c.x;
	is >> c.y;
	return is;
}


int main()
{

	Point2D p1(10, 10);
	Point2D p2(10, 10);
	Point2D p3;

	p3 = p1 + p2;
	p3 == Point2D(20,20);
	p3 = p1 - p2;
	p3 = p1 * (float)3; 
	p1 += p2;
	p1 == Point2D(20, 20); 
	p2 *= 2;
	p2 == Point2D(20, 20); 
	p1 /= 3;
	p1 -= p2;
	p2 = p1 * (float)1.352;
	p3 /= 1.99;

	std::cout << "P1: " << p1 << std::endl << "P2: " << p2 << std::endl <<"P3: " << p3 << 	std::endl;
	double length = p1;

	std::cout << "odleglosc punktu P1 od punktu (0, 0): " << length << std::endl;


}
