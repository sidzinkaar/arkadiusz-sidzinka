#ifndef Settings_h
#define Settings_h
#include <iostream>
#include <string>
#include <fstream>



class Settings
{
private:
    Settings();
	std::string connectionString;
	std::string path;
	

public:
	std::string getConnectionString(){ return connectionString; }
	std::string getPath() { return path; }
	static Settings& getInstance();
	void UstawPolaczenieZBazaDanych(std::string);
	void UstawPlikKonfiguracyjny(std::string);
	static int counter;
};
#endif
