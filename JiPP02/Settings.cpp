#include "Settings.h"

Settings::Settings()
{
}

Settings& Settings::getInstance()
{
	static Settings object;
	counter++;
	return object;
}

void Settings::UstawPolaczenieZBazaDanych(std::string connectionString)
{
	this->connectionString = connectionString;
	std::cout << "Polaczenie z baza danych zostalo nawiazane" << std::endl;
}

bool exists (const std::string& name) 
{
    std::ifstream f(name.c_str());
    return f.good();
}

void Settings::UstawPlikKonfiguracyjny(std::string path)
{
	std::string filePath = path + "config.cfg";
	if(exists(filePath) == 1)
	{
		this->path = path;
		std::cout << "Plik konfiguracyjny zostal ustawiony" << std::endl;
	}
	else
		std::cout << "Nie znaleziono pliku config.cfg" << std::endl;
}



int Settings::counter = 0;

int main()
{
	Settings::getInstance();
	Settings::getInstance().UstawPlikKonfiguracyjny("/home/yourney/Arekjipp/");
	Settings::getInstance().UstawPolaczenieZBazaDanych("Data Source=(LocalDb)\\MSSQLLocalDB");

	std::cout << std::endl << "Sciezka do pliku config.cfg: " << Settings::getInstance().getPath() << std::endl;
	std::cout << "Connection String: " << Settings::getInstance().getConnectionString() << std::endl;
	std::cout << "Licznik: " << Settings::counter << std::endl;

	

	return 0;
}

