#ifndef LIST_H
#define LIST_H
template<class T>
class List
{
	public:
		List();
		~List() {}
		bool isEmpty();
		void push_back(T data);
		void pop_front();
		void push_front(T data);
		void pop_back();
		void empty();
		int size();
		void insert(int position, T data);
		T front();
		T back();
     	void show()
		{
			Node *tmp = frontPointer;
			while(tmp != NULL)
			{
				std::cout << tmp->date << std::endl;
				tmp = tmp->next;
			}
			std::cout << "count: " << count << std::endl;
		}

	private:
		struct Node
		{
			T date;
			Node *next;
			Node *prev;
		};

		Node *frontPointer;
		Node *backPointer;
		int count;

};
#endif
