#ifndef QUEUE_H
#define QUEUE_H
template<class T>
class Queue
{
	public:
		Queue();
		bool Empty();
		void push_back(T data);
		void pop_front();
		int size();
		T front();
		T back();

	private:
		struct Node
		{
			T date;
			Node *next;
		};

		Node *frontPointer;
		Node *backPointer;
		int count;

};
#endif
