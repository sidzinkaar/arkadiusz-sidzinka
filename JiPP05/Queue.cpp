#include <iostream>
#include "Queue.h"

template<class T>
T Queue<T>::front()
{
	return frontPointer->date;
}

template<class T>
T Queue<T>::back()
{
	return backPointer->date;
}

template<class T>
Queue<T>::Queue()
{
	frontPointer = NULL;
	backPointer = NULL;
	count = 0;
}

template<class T>
bool Queue<T>::Empty()
{
	if (count == 0)
		return 1;
	else return 0;
}

template<class T>
void Queue<T>::push_back(T data)
{
	Node *newNode = new Node;
	newNode->date = data;
	newNode->next = NULL;

	if(Empty() == 1)
	{
		frontPointer = newNode;
	}
	else
	{
		backPointer->next = newNode;
	}
	
	backPointer = newNode;
	
	count++;
}

template<class T>
void Queue<T>::pop_front()
{
	if(Empty() == 1)
	{
		std::cout << "Kolejka jest już pusta" << std::endl;
	}
	else
	{
		if(frontPointer == backPointer)
		{
			frontPointer = NULL;
			backPointer = NULL;
		}
		else
		{		
			frontPointer = frontPointer->next;
		}
	}
	
	count--;
}

template<class T>
int Queue<T>::size()
{
	return count;
}

int main()
{
	Queue<int> line;

	line.push_back(2);
	
	line.push_back(3);
	line.push_back(20);
	
	
	std::cout << "przod kolejki: " << line.front() << std::endl;
	std::cout << "tyl kolejki: " << line.back() << std::endl;
	std::cout << "wielkosc kolejki: " << line.size() << std::endl;
	
	
	
	return 0;
}
