#include <iostream>
#include "List.h"

template<class T>
List<T>::List()
{
	frontPointer = NULL;
	backPointer = NULL;
	count = 0;
}

template<class T>
T List<T>::front()
{
	return frontPointer->date;
}

template<class T>
T List<T>::back()
{
	return backPointer->date;
}

template<class T>
int List<T>::size()
{
	return count;
}

template<class T>
bool List<T>::isEmpty()
{
	if (count == 0)
		return 1;
	else return 0;
}

template<class T>
void List<T>::empty()
{
	while(frontPointer->next != NULL)
	{
		frontPointer = frontPointer->next;
		frontPointer->prev = NULL;
	}
	frontPointer = NULL;
	count = 0;
}

template<class T>
void List<T>::push_back(T data)
{
	Node *newNode = new Node;
	newNode->date = data;
	newNode->next = NULL;
	newNode->prev = backPointer;

	if(isEmpty() == 1)
	{
		frontPointer = newNode;
	}
	else
	{
		backPointer->next = newNode;
	}
	
	backPointer = newNode;
	
	count++;
}

template<class T>
void List<T>::push_front(T data)
{
	Node *newNode = new Node;
	newNode->date = data;
	newNode->next = frontPointer;
	newNode->prev = NULL;

	if(isEmpty() == 1)
	{
		frontPointer = newNode;
		backPointer = newNode;
	}
	else
	{
		frontPointer->prev = newNode;
	}
	
	frontPointer = newNode;
	
	count++;
}

template<class T>
void List<T>::pop_front()
{
	if(isEmpty() == 1)
	{
		std::cout << "Lista jest już pusta" << std::endl;
	}
	else
	{
		if(frontPointer == backPointer)
		{
			frontPointer = NULL;
			backPointer = NULL;
		}
		else
		{
			frontPointer = frontPointer->next;
			frontPointer->prev = NULL;
		}
	}
	
	count--;
}

template<class T>
void List<T>::pop_back()
{
	if(isEmpty() == 1)
	{
		std::cout << "Lista jest już pusta" << std::endl;
	}
	else
	{
		if(frontPointer == backPointer)
		{
			frontPointer = NULL;
			backPointer = NULL;
		}
		else
		{
			backPointer = backPointer->prev;
			backPointer->next = NULL;
		}
	}
	
	count--;
}

template<class T>
void List<T>::insert(int position, T data) //wstawia element na okreslonej pozycji (0 - pierwsza pozycja, 1 druga itd.)
{
	Node *tmp = frontPointer;
	Node *tmp2 = new Node;
	tmp2->date = data;
	
	if(position == 0)
	{
		
		tmp2->next = frontPointer;
		tmp2->prev = NULL;
		frontPointer = tmp2;
	}
	else
	{
		for(int i = 0; i < position - 1; i++)
		{
			tmp = tmp->next;
		}
		tmp2->next = tmp->next;
		tmp->next = tmp2;
		tmp2->prev = tmp;
	}
	
	count++;
	
	
	
	
}
	
int main()
{
	List<int> line;

	
	line.push_back(3);
	line.push_back(4);
	
	line.push_back(5);
	line.push_front(2);
	line.push_front(1);

	line.insert(3, 150);
	
	
	std::cout << "Lista: " << std::endl;
	line.show();
	std::cout << "Wielkosc listy: " << line.size() << std::endl;

	line.empty();
	std::cout << std::endl << line.isEmpty() << std::endl;

	
	
	
	return 0;
}
