#include<iostream>

int m,n,p,q;

template<typename T>
void mul(T **A, T **B, T **C)
{
	for(int i=0; i<m; i++)
	{
		for(int j=0; j<q; j++)
    		{
     			C[i][j]=0;
     			for(int k=0; k<n; k++)
       			{
       				C[i][j] += A[i][k] * B[k][j];
     			}
     		}	
	}
}

int main()
{
	double **tabA, **tabB, **tabC;
	std::cout << "Podaj liczbe wierszy 1. macierzy: ";
	std::cin >> m;
	std::cout << "Podaj liczbe kolumn 1. macierzy: ";
	std::cin >> n;
	std::cout << "Podaj liczbe wierszy 2. macierzy: ";
	std::cin >> p;
	std::cout << "Podaj liczbe kolumn 2. macierzy: ";
	std::cin >> q;

	if(n == p)
	{
		tabA = new double* [m];
		for (unsigned i = 0; i < m; i++)
			tabA[i] = new double [n];
 
		tabB = new double* [p];
		for (unsigned i = 0; i < p; i++)
			tabB[i] = new double [q];
 	
		tabC = new double* [m];
		for (unsigned i = 0; i < m; i++)
			tabC[i] = new double [q];

		for(int i = 0; i < m; i++)
		{
			for(int j = 0; j < n; j++)
			{
				tabA[i][j] = 2.32;
			}
		}

		for(int i = 0; i < p; i++)
		{
			for(int j = 0; j < q; j++)
			{
				if(i == j)
					tabB[i][j] = 1;
				else
					tabB[i][j] = 0;
			}
		}

			

		mul(tabA, tabB, tabC);

		std::cout << "Macierz A: " << std::endl;
		for(int i = 0; i < m; i++)
		{
			for(int j = 0; j < n; j++)
			{
				std::cout << tabA[i][j] << " ";
			}
			std::cout << std::endl;
		}
		std::cout << std::endl;
		
		std::cout << "Macierz B: " << std::endl;
		for(int i = 0; i < p; i++)
		{
			for(int j = 0; j < q; j++)
			{
				std::cout << tabB[i][j] << " ";
			}
			std::cout << std::endl;
		}
		std::cout << std::endl;
	
		std::cout << "Macierz po wymnozeniu: " << std::endl;
		for(int i = 0; i < m; i++)
		{
			for(int j = 0; j < q; j++)
			{
				std::cout << tabC[i][j] << " ";
			}
			std::cout << std::endl;
		}
	}
	else 
		std::cout << "Nie mozna wykonac mnozenia macierzy!" << std::endl;

	
	

	return 0;
}
